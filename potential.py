import numpy as np
import properties as prop
import energy_varPhi as en_var
import energy_constPhi as en_const
import matplotlib.pyplot as plt
from scipy import optimize

phi_list=np.linspace(1e-6,0.85,100) #Phi

x0 = [1.0, 0.5] 
rmbounds = (0.01,5.0)
tbounds = (0.001,3.14)
nbounds = (1,10)

fs = prop.fs
fm = prop.fm
phitot = 1000
k_agg = prop.k_agg
k_bind = prop.k_bind
Cp = prop.Cp
n=prop.n_spine
phisat = prop.phisat
khat = prop.khat
khat_KBT = prop.khat_KBT
k = 63 * 1.38e-23 * 300
KBT = prop.KBT
phi_var = False
sign = "-"
if phi_var == True:
  ret = optimize.minimize( en_var.total_energy, x0=x0, bounds = (rmbounds,tbounds), args=(phitot, phisat, khat, fs, n, k, k_agg),method = 'L-BFGS-B' )
  rm = ret.x[0] * 1e-6
  theta = ret.x[0]
else:
  chem_pot_rec = []  
  for i in range(len(phi_list)):
    x0 = [0.5]  
    bounds = [tbounds]
    phi = phi_list[i]
    ret = optimize.minimize( en_const.total_energy, x0=x0, bounds = bounds, args=(phitot, phi, phisat, khat, fs, n, k, k_agg),method = 'L-BFGS-B' )
    theta = ret.x[0]
    rm = np.sqrt(phitot/(phisat * phi * 2 * np.pi * (1 - np.cos(theta))))
    rp = rm * 1e6 * np.sin(theta)
    area =  2 * np.pi * rm * rm * (1 - np.cos(theta))
    phi = phitot/(area * phisat)
    #Definition of chemical potential
    Hmean = 1/rm
    theta = theta
    if sign == "-":
      chem_pot = -( (0.5 * ((1.0/phisat) * khat_KBT) * (Hmean - Cp )**2 + (0.5 * khat_KBT/phisat**0.5) * phi_list[i]**0.5 * (Hmean - Cp) * np.sqrt(en_var.n_spine * 2 * 3.14 * (1 - np.cos(theta))/(phitot+1.0e-3))   + np.log(phi_list[i]) + ( (k_agg) * phi_list[i] + 15 * phi_list[i]**2 + 24.48 * phi_list[i]**3 + 35.3 * phi_list[i]**4) - k_bind) + (1/(phisat * phi)) * en_const.total_energy([theta], phitot, phi, phisat, khat, fs, n, k, k_agg)/(1e16*area))
    else:
      chem_pot = ( (0.5 * ((1.0/phisat) * khat_KBT) * (Hmean - Cp )**2 + (0.5 * khat_KBT/phisat**0.5) * phi_list[i]**0.5 * (Hmean - Cp) * np.sqrt(en_var.n_spine * 2 * 3.14 * (1 - np.cos(theta))/(phitot+1.0e-3))   + np.log(phi_list[i]) + ( (k_agg) * phi_list[i] + 15 * phi_list[i]**2 + 24.48 * phi_list[i]**3 + 35.3 * phi_list[i]**4) - k_bind) + (1/(phisat * phi)) * en_const.total_energy([theta], phitot, phi, phisat, khat, fs, n, k, k_agg)/(1e16*area))
      print((1/(phisat * phi)) * en_const.total_energy([theta], phitot, phi, phisat, khat, fs, n, k, k_agg)/(1e16*area))
    chem_pot_rec.append(chem_pot)

func_pot_non_eq = (1/0.06) * 0.1 * np.exp( chem_pot_rec ) 

plt.figure(100)
plt.xlabel("$\phi$",fontsize=18)
plt.ylabel("Kf",fontsize=18)
plt.plot(phi_list, func_pot_non_eq)
plt.figure(101)
plt.xlabel("$\phi$",fontsize=18)
plt.ylabel("Non-dim Chem. Potential",fontsize=18)
if sign == "-":
  for i in range(len(chem_pot_rec)):
     chem_pot_rec[i] = -chem_pot_rec[i]
plt.plot(phi_list, chem_pot_rec)
plt.show()
