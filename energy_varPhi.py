import numpy as np
import properties as prop

def entropy( area, phitot, phisat, khat, fs, k ):
    # Returns entropy per unit area
    ph_en = phitot/ (area * phisat)
    #entropy = (kentropy/phisat) * (ph_en*np.log(ph_en) - ph_en - 0.5 * ph_en**2 + ph_en**2 + 0.5 * ph_en**3)
    entropy = KBT * phisat * (ph_en*np.log(ph_en) - ph_en)
    return entropy


def aggregation_energy(rm, theta, phitot, phisat, k_agg, area):
    ph_en = (phitot)/ (area * phisat)
    return area * (KBT*phisat) * ( 0.5 * k_agg * ph_en**2 + 5 * ph_en**3 + 6.12 * ph_en**4 + 7.06 * ph_en**5)- area * phisat * k_bind * KBT * ph_en


def mismatch( rm, phitot, phisat, khat, fs, k ):
    # Note that the  area term cancels out with the phitot/(area)
    return 0.5 * khat * (phitot/phisat) * (1/rm - Cp) * (1/rm - Cp)


def fp_tension( rp, rm, theta, khat, fs, k ):
    r0 = (rp + rm) * np.sin( theta )
    return 2 * np.pi * rp * fs * (r0*theta + rp * ( np.cos( theta ) - 1 ) )

def fm_tension( rm, theta, phitot, phisat, khat, fs, k ):
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    return area * fs

def Fplus( rp, rm, theta, khat, fs, k ):
    r0 = (rp + rm) * np.sin( theta )
    ret = 2 * np.pi * (k * theta )
    return ret

def Fminus( rm, theta, phitot, phisat, khat, fs, k ):
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    Fm = mismatch( rm, phitot, phisat, khat, fs, k) + area * ( k/(2*rm*rm) + entropy( area, phitot, phisat, khat, fs, k ) )
    return Fm

def Fzero( rp, rm, theta, n, khat, fs, k ):
    r0 = (rp + rm) * np.sin( theta )
    aDend = 2 * np.pi * dendDia * xtot - n * np.pi * r0 * r0
    f0 = aDend * ( fs + fm )
    return f0

def total_energy(x,phitot, phisat, khat, fs, n, k, k_ag_en):
    rm = x[0] * 1e-6
    theta = x[1]
    rp = rm * np.sin(theta)
    phitot = phitot/n
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    Fp = Fplus( rp, rm, theta, khat, fs, k )
    Fm = Fminus( rm, theta, phitot, phisat, khat, fs, k )
    F0 = Fzero( rp, rm, theta, n, khat, fs, k )
    agg_en = aggregation_energy(rm, theta, phitot, phisat, k_ag_en, area)
    basal_tension_energy = ( fs + fm ) * 2 * np.pi * dendDia * xtot
    total = n * ( Fm + Fp ) + membrane_tension(rm,theta,phisat,phitot,n,khat,fs, k) + n * agg_en - basal_tension_energy
    return total * 1e16 

def membrane_tension(x,y,phitot,phisat,n,khat,fs, k):
    rm = x * 1.0e-6
    theta = y
    rp = rm * np.sin(theta)
    F0 = Fzero( rp, rm, theta, n, khat, fs, k )
    fm_ten = n * fm_tension( rm, theta, phitot, phisat, khat, fs, k )
    fp_ten = n * fp_tension( rp, rm, theta, khat, fs, k )
    mem_ten = F0 + fm_ten + fp_ten
    return mem_ten

def bending(x,y,phitot,n,khat,fs):
    rm = np.asarray(x) * 1.0e-6
    theta = y
    rp = rm * np.sin(theta)
    #Check area
    area_m = np.pi * rm * rm * ( 1 - np.cos( theta ) )
    r0 = (rp + rm) * np.sin( theta )
    fp_bend = 2 * np.pi * k * theta
    fm_bend = area_m *  k/(2*rm*rm)
    mem_bend = fp_bend + fm_bend
    return mem_bend * 1e16


fs = prop.fs
fm = prop.fm
phitot = 1000
k_agg = prop.k_agg
k_bind = prop.k_bind
Cp = prop.Cp
n=prop.n_spine
phisat = prop.phisat
khat = prop.khat
khat_KBT = prop.khat_KBT
k = 63 * 1.38e-23 * 300
KBT = prop.KBT
dendDia = prop.dendDia
xtot = prop.xtot
n_spine = prop.n_spine
