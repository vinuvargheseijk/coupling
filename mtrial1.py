########################################################################
# This example illustrates molecular transport along a closed-end 
# cylinder. All the molecules start out in a single voxel A[0] on the left,
# and all end up at A[99], on the right.
########################################################################

import moose
import numpy as np
import pylab
import rdesigneur as rd

def RealChemPotKf(D,xi,d,mum):
    DiffTau = xi**2/(2*D)
    G2 = (1/mum)**2 * ( np.exp(mum) - mum - 1 )
    G1 = (1/mum) * ( np.exp(mum) - 1 )
    #KpTau = ( xi**2 * 1 / ( (xi - d)**2 + 2 * d**2 * G2  - 2*d * (xi - d) * G1) )
    Kp = ( xi**2 * 1 / ( (xi - d)**2 + 2 * d**2 * G2  - 2*d * (xi - d) * G1) ) / DiffTau
    return Kp

def RealChemPotKb(D,xi,d,mum):
    DiffTau = xi**2/(2*D)
    G2 = (1/mum)**2 * ( np.exp(-mum) + mum - 1 )
    G1 = (1/mum) * ( np.exp(-mum) - 1 )
    #KpTau = ( xi**2 * 1 / ( (xi - d)**2 + 2 * d**2 * G2  - 2*d * (xi - d) * G1) )
    Kp = ( xi**2 * 1 / ( (xi - d)**2 + 2 * d**2 * G2  + 2*d * (xi - d) * G1) ) / DiffTau
    return Kp

moose.Neutral( '/library' )
moose.Neutral( '/library/cm' )
moose.CubeMesh( '/library/cm/dend' )
cytosol = moose.Pool( '/library/cm/dend/cytosol' )
membrane = moose.Pool( '/library/cm/dend/membrane' )
potential = moose.Pool( '/library/cm/dend/potential' )
reacF = moose.Reac( '/library/cm/dend/reacF' )
reacB = moose.Reac( '/library/cm/dend/reacB' )
cytosol.diffConst = 1e-12
CytD = cytosol.diffConst
membrane.diffConst = 1e-13
potential.diffConst = 1e-12
cytosol.motorConst = 0     # Metres/sec
moose.connect( reacF, 'sub', cytosol, 'reac' )
moose.connect( reacF, 'prd', membrane, 'reac' )
moose.connect( reacB, 'sub', membrane, 'reac' )
moose.connect( reacB, 'prd', cytosol, 'reac' )
reacF.Kf = 0
reacF.Kb = 0
reacB.Kf = 0
reacB.Kb = 0
pot_d = 0.5e-6   #influence zone of potential from membrane
cytZ = 0.25e-6   #distance from membrane
cytZ2 = cytZ**2
Z_d = cytZ - pot_d
Z_d2 = Z_d * Z_d
funcF = moose.Function('/library/cm/dend/funcF')
funcF.x.num=1
funcB = moose.Function('/library/cm/dend/funcB')
funcB.x.num=1
func_pot = moose.Function('/library/cm/dend/func_pot')
func_pot.x.num=1
phisat = 1/50e-18
khat = 35 * 1.38e-23 * 300
n_spine = 1
Cp = 1/20e-9
theta = 0.5
Rd=1e-6
voxel_area = 2*np.pi*0.5e-6*0.5e-6  #2 pi * radius * diffusionLength
f_m =1/(phisat*voxel_area)          #Fraction of membrane area
print("Voxel Area", voxel_area)
mum=1
funcF.expr = str(cytZ2) + "/(" + str(Z_d**2) + "+" + str(2*pot_d**2) + "* ((1/("+str(f_m)+"*x0))^2 * (exp("+str(f_m)+"*x0) -" + str(f_m) + "*x0 - 1 )) + " + str(2*pot_d*Z_d) + "*(1/("+str(f_m)+"*x0)) * ( exp("+str(f_m)+"*x0) - 1 ))*(2*"+str(CytD)+"/"+str(cytZ2)+")"
funcB.expr = str(cytZ2) + "/(" + str(Z_d**2) + "+" + str(2*pot_d**2) + "* ((1/("+str(f_m)+"*x0))^2 * (exp("+str(-f_m)+"*x0) +" + str(f_m) + "*x0 - 1 )) + " + str(2*pot_d*Z_d) + "*(1/("+str(f_m)+"*x0)) * ( exp("+str(-f_m)+"*x0) - 1 ))*(2*"+str(CytD)+"/"+str(cytZ2)+")"
func_pot.expr = str(voxel_area) + " * ( (0.5 * " + str(khat) + "*( (1/"+str(Rd)+") -" + str(Cp) + ")^2) + " + str(khat*f_m) + "* x0 * ( (1/"+str(Rd)+") -" +str(Cp)+")*(" + str(np.sqrt(phisat * n_spine * 2 * np.pi * (1-np.cos(theta)))) + "/sqrt(x0))" + "* 0.5 * (("+str(f_m)+"*x0)^-0.5) )"
print(funcF.expr)
print(func_pot.expr)
moose.connect(membrane,'nOut',funcF.x[0],'input')
moose.connect(funcF,'valueOut',reacF,'setNumKf')
moose.connect(membrane,'nOut',funcB.x[0],'input')
moose.connect(funcB,'valueOut',reacB,'setNumKf')
moose.connect(cytosol,'nOut',func_pot.x[0],'input')
moose.connect(func_pot,'valueOut',potential,'setN')

rdes = rd.rdesigneur(
    turnOffElec = True,
    #This subdivides the length of the soma into 0.5 micron voxels
    diffusionLength = 0.5e-6, 
    cellProto = [['somaProto', 'soma', 1e-6, 50e-6]],
    chemProto = [['cm', 'cm']],
    chemDistrib = [['cm', 'soma,dend#', 'install', '1' ]],
    plotList = [
        ['soma', '1', 'dend/potential', 'conc', 'Concentration of cytosol'],
        ['soma', '1', 'dend/potential', 'conc', 'Concentration of membrane', 'wave'],
    ],
    moogList = [['soma', '1', 'dend/membrane', 'conc', 'membrane Conc', 0, 20 ]]
)

rdes.buildModel()
moose.element( '/model/chem/dend/cytosol[50]' ).concInit = 0.1
moose.element( '/model/chem/dend/membrane' ).concInit = 0.001
moose.reinit()
#moose.start(10)
rdes.displayMoogli( 1, 2, rotation = 0, azim = -np.pi/2, elev = 0.0 )
