########################################################################
# This example illustrates molecular transport along a closed-end 
# cylinder. All the molecules start out in a single voxel A[0] on the left,
# and all end up at A[99], on the right.
########################################################################
import pandas as pd
import moose
import numpy as np
import pylab
import matplotlib.pyplot as plt
import rdesigneur as rd
from scipy import optimize


###Has to include dA/dphi too

def plot_fn(x,vec):
    df = pd.DataFrame()
    df["x"] = x
    df["vec"] = vec
    return df

def entropy( area, phitot, phisat, khat, fs, k ):
    # Returns entropy per unit area
    ph_en = phitot/ (area * phisat)
    #entropy = (kentropy/phisat) * (ph_en*np.log(ph_en) - ph_en - 0.5 * ph_en**2 + ph_en**2 + 0.5 * ph_en**3)
    entropy = KBT * phisat * (ph_en*np.log(ph_en) - ph_en)
    return entropy


def aggregation_energy(rm, theta, phitot, phisat, k_agg, area):
    ph_en = (phitot)/ (area * phisat)
    return area * (KBT*phisat) * ( 0.5 * k_agg * ph_en**2 + 5 * ph_en**3 + 6.12 * ph_en**4 + 7.06 * ph_en**5)- area * phisat * k_bind * KBT * ph_en


def mismatch( rm, phitot, phisat, khat, fs, k ):
    # Note that the  area term cancels out with the phitot/(area)
    return 0.5 * khat * (phitot/phisat) * (1/rm - Cp) * (1/rm - Cp)


def fp_tension( rp, rm, theta, khat, fs, k ):
    r0 = (rp + rm) * np.sin( theta )
    return 2 * np.pi * rp * fs * (r0*theta + rp * ( np.cos( theta ) - 1 ) )

def fm_tension( rm, theta, phitot, phisat, khat, fs, k ):
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    return area * fs

def Fplus( rp, rm, theta, khat, fs, k ):
    r0 = (rp + rm) * np.sin( theta )
    ret = 2 * np.pi * (k * theta )
    return ret

def Fminus( rm, theta, phitot, phisat, khat, fs, k ):
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    Fm = mismatch( rm, phitot, phisat, khat, fs, k) + area * ( k/(2*rm*rm) + entropy( area, phitot, phisat, khat, fs, k ) )
    return Fm

def Fzero( rp, rm, theta, n, khat, fs, k ):
    r0 = (rp + rm) * np.sin( theta )
    aDend = 2 * np.pi * dendDia * xtot - n * np.pi * r0 * r0
    f0 = aDend * ( fs + fm )
    return f0

def total_energy(x,phitot, phisat, khat, fs, n, k, k_ag_en):
    rm = x[0] * 1e-6
    theta = x[1]
    rp = rm * np.sin(theta)
    phitot = phitot/n
    area = 2 * np.pi * rm * rm * ( 1 - np.cos( theta ) )
    Fp = Fplus( rp, rm, theta, khat, fs, k )
    Fm = Fminus( rm, theta, phitot, phisat, khat, fs, k )
    F0 = Fzero( rp, rm, theta, n, khat, fs, k )
    agg_en = aggregation_energy(rm, theta, phitot, phisat, k_ag_en, area)
    basal_tension_energy = ( fs + fm ) * 2 * np.pi * dendDia * xtot
    total = n * ( Fm + Fp ) + membrane_tension(rm,theta,phisat,phitot,n,khat,fs, k) + n * agg_en - basal_tension_energy
    return total * 1e16 

def membrane_tension(x,y,phitot,phisat,n,khat,fs, k):
    rm = x * 1.0e-6
    theta = y
    rp = rm * np.sin(theta)
    F0 = Fzero( rp, rm, theta, n, khat, fs, k )
    fm_ten = n * fm_tension( rm, theta, phitot, phisat, khat, fs, k )
    fp_ten = n * fp_tension( rp, rm, theta, khat, fs, k )
    mem_ten = F0 + fm_ten + fp_ten
    return mem_ten

def bending(x,y,phitot,n,khat,fs):
    rm = np.asarray(x) * 1.0e-6
    theta = y
    rp = rm * np.sin(theta)
    #Check area
    area_m = np.pi * rm * rm * ( 1 - np.cos( theta ) )
    r0 = (rp + rm) * np.sin( theta )
    fp_bend = 2 * np.pi * k * theta
    fm_bend = area_m *  k/(2*rm*rm)
    mem_bend = fp_bend + fm_bend
    return mem_bend * 1e16

def dendShape( theta, rp, rm ):
    dth = theta / 50
    xbend = 2 * np.sin( theta ) * (rp + rm )
    Lflat = (xtot * 1e6 - xbend ) / 2.0
    x = [0.0, Lflat]
    y = [0.0, 0.0]
    # To show centre of curvature
    x.extend( [Lflat, Lflat] )
    y.extend( [rp, 0.0] )
    x.extend( [ Lflat + rp * np.sin( th + dth ) for th in np.arange(0.0, theta * 0.999999, dth ) ] )
    y.extend( [ rp * (1-np.cos( th + dth) ) for th in np.arange(0.0, theta * 0.999999, dth ) ] )
    xlast = x[-1]
    ylast = y[-1]
    xoffset = rm * np.sin( theta ) + xlast
    #xoffset = xtot / 2.0
    yoffset = -rm * np.cos(theta) + ylast
    #print('rm={:.4g} um, rp={:.4g} um, theta={:.4g} rad'.format( rm, rp, theta) )
    #print( xlast, ylast, xoffset, yoffset )
    x.append( xoffset )
    y.append( yoffset )

    x.extend( [ xoffset - rm * np.sin( th ) for th in np.arange (theta, 0, -dth ) ] )
    y.extend ([yoffset + rm * np.cos( th ) for th in np.arange (theta, 0, -dth ) ] )
    xlast = x[-1]
    ylast = y[-1]
    x.extend( [ xtot * 1e6 - i for i in x[::-1] ] )
    y.extend( y[::-1] )
    return np.array( x ), np.array( y )
#usage x, y = dendShape( theta, rp, rm)

def diff_profile(x,t,diff):
    y = (100/t**0.5) * np.exp(-x**2/(4 * diff * t))
    return y

def find_exponent(c):
    t = ''
    b = str(c)
    for i in range(1,len(b)):
     k = len(b) - i
     if (b[k]) == 'e':
         break
     else:
         t = t + b[k]
    return int(t[::-1])    


moose.Neutral( '/library' )
moose.Neutral( '/library/cm' )
moose.CubeMesh( '/library/cm/dend' )
cytosol = moose.Pool( '/library/cm/dend/cytosol' )
membrane = moose.Pool( '/library/cm/dend/membrane' )
threshold = moose.Pool( '/library/cm/dend/threshold' )
mem_area = moose.Pool( '/library/cm/dend/mem_area' )
Hmean = moose.Pool( '/library/cm/dend/Hmean' )
theta = moose.Pool( '/library/cm/dend/theta' )
f_m = moose.Pool( '/library/cm/dend/f_m' )
full_pot = moose.Pool( '/library/cm/dend/full_pot' )
Sphi = moose.Pool( '/library/cm/dend/Sphi' )
fneq= moose.Pool( '/library/cm/dend/fneq' )
feq= moose.Pool( '/library/cm/dend/feq' )
reacF = moose.Reac( '/library/cm/dend/reacF' )
reacB = moose.Reac( '/library/cm/dend/reacB' )
cytosol.diffConst = 1e-12
CytD = cytosol.diffConst
membrane.diffConst = 0.0
Sphi.diffConst = 0.0
threshold.diffConst = 0.0
diffL = 50e-9
dendR = 0.5e-6
dendDia = 2 * dendR
memt = 5e-9
Length = 100
xtot = Length * 1e-6
moose.connect( reacF, 'sub', cytosol, 'reac' )
moose.connect( reacF, 'prd', membrane, 'reac' )
moose.connect( reacB, 'sub', membrane, 'reac' )
moose.connect( reacB, 'prd', cytosol, 'reac' )
reacF.Kf = 0
reacF.Kb = 0
reacB.Kf = 0
reacB.Kb = 0
k_agg = -60
k_bind = -5
#func_pot_init = moose.Function('/library/cm/dend/func_pot_init')
func_pot_non_eq = moose.Function('/library/cm/dend/func_pot_non_eq')
func_pot = moose.Function('/library/cm/dend/func_pot')
func_phi = moose.Function('/library/cm/dend/func_phi')
func_kf = moose.Function('/library/cm/dend/func_kf')
func_kb = moose.Function('/library/cm/dend/func_kb')
func_area = moose.Function('/library/cm/dend/func_area')
func_area.x.num = 1
func_kf.x.num = 1
func_kb.x.num = 0
func_phi.x.num = 2
#func_pot_init.x.num=0
func_pot_non_eq.x.num=4
func_pot.x.num = 1
phisat = 1/50e-18
khat = 35 * 1.38e-23 * 300
k = 63 * 1.38e-23 * 300
KBT = 1.38e-23*300
khat_KBT = 35
n_spine = 1
Cp = 1/20e-9
fs = 5.5e-6
fm = 0.0
voxel_area = 2*np.pi*dendR * diffL  #2 pi * radius * diffusionLength
func_area.expr = "x0"
moose.connect(f_m, 'nOut', func_area.x[0],'input')
moose.connect(func_area, 'valueOut', mem_area,'setN')
Vmem = np.pi * (dendR**2 - (dendR - memt)**2) * diffL  #Volume of membrane 2 * pi * mem_thickness^2 * voxel_length
Vcyt = np.pi * (dendR - memt) * (dendR - memt) * diffL #Volume of cytosol = Volume of cylindrical voxel
print("Voxel Volume", Vcyt)
print("Membrane Volume", Vmem)
Vratio = Vmem/Vcyt
basal_area  = np.pi * 0.5e-6**2 * diffL
print("BASAL AREA", basal_area)
func_phi.expr =  "(x0/((x1 + 20e-18) * "+ str(phisat) + "))"
#func_phi.expr = "0*x0 + 0*x1 + 0.0"
print("phi evaluation function",func_phi.expr,"/n")

#The following expression gives the association constant
sign="-"

if sign == "-":
    #There is an offset of 0.1 in log to prevent NaN
   func_pot_non_eq.expr = "(x3>1) * exp(-(0.5 * ("+str((1.0/phisat) * khat_KBT)+") * (x1 - " + str(Cp) + ")^2 + " + str(0.5 * khat_KBT/phisat**0.5) + " * x0^0.5 * (x1 - " + str(Cp) + ") * sqrt(" + str(n_spine) + " * 2 * 3.14 * (1 - cos(x2))/(x3+1.0e-3))   + log(x0+0.1) + (" +str(k_agg) + " * x0 + 15 * x0^2 + 24.48 * x0^3 + 35.3 * x0^4) - " + str(k_bind) + ")) + (x3<1) * exp(1 * " + str(k_bind) + ")"

   #func_pot_non_eq.expr = "0*x0 + 0* x1+0*x2+0*x3 + (x3>1) * exp(-( log (x0) + (" +str(k_agg) + " * x0 + 15* x0^2 + 24.48 * x0^3 + 35.3 * x0^4) - " + str(k_bind) + ") ) + (x3<1) * exp(-" + str(-k_bind) + ")"
   #func_pot_non_eq.expr = "0*x0 + 0* x1+0*x2+0*x3 + (x3>1) * exp(-( log(x0) + (" +str(k_agg) + " * x0 + 15* x0^2 + 24.48 * x0^3 + 35.3 * x0^4) - " + str(k_bind) + ") ) + (x3<1) * exp(-" + str(-k_bind) + ")"
   #func_pot_non_eq.expr = "0*x0 + 0* x1+0*x2+0*x3 + (x3>1) * exp(-( log(x0+0.1) - " + str(k_bind) + ") ) + (x3<1) * exp(-" + str(-k_bind) + ")"
else:
   func_pot_non_eq.expr = "(x3>1) * exp((0.5 * ("+str((1.0/phisat) * khat_KBT)+") * (x1 - " + str(Cp) + ")^2 + " + str(0.5 * khat_KBT/phisat**0.5) + " * x0^0.5 * (x1 - " + str(Cp) + ") * sqrt(" + str(n_spine) + " * 2 * 3.14 * (1 - cos(x2))/(x3+1.0e-3))   + log(x0) + (" +str(k_agg) + " * x0 + 15 * x0^2 + 24.48 * x0^3 + 35.3 * x0^4)))* exp( -1*" +str(k_bind) + ") + (x3<1) * exp( -1*" +str(k_bind) + ")"
func_pot.expr = "x0"

print("Expression",func_pot_non_eq.expr)
f_cytosol = 0.75 #fraction in cytosol
f_membrane = 0.15 #fraction in membrane
f_cm = 0.1  #ratio
func_kf.expr = "(1/0.06)*x0 * " + str(f_cm)
func_kb.expr = "(1/0.06)"
#Here membrane nOut is supposed to be the phitot. But phitot is the sum of nonzero membrane voxel, and hence there is an inconsistency. Probably the Sphi should also be calculated in pyrun?
moose.connect(threshold,'nOut',func_phi.x[0],'input')
#moose.connect(membrane,'nOut',func_phi.x[0],'input')
moose.connect(f_m,'nOut',func_phi.x[1],'input')
moose.connect(func_phi,'valueOut',Sphi,'setN')


moose.connect(Sphi,'nOut',func_pot_non_eq.x[0],'input')
moose.connect(Hmean,'nOut',func_pot_non_eq.x[1],'input')
moose.connect(theta,'nOut',func_pot_non_eq.x[2],'input')
moose.connect(threshold,'nOut',func_pot_non_eq.x[3],'input')
#moose.connect(membrane,'nOut',func_pot_non_eq.x[3],'input')
moose.connect(func_pot_non_eq,'valueOut',fneq,'setN')

moose.connect(fneq,'nOut',func_pot.x[0],'input')
moose.connect(func_pot,'valueOut',full_pot,'setN')

moose.connect(func_kb,'valueOut',reacB,'setNumKf')

moose.connect(full_pot,'nOut',func_kf.x[0],'input')
moose.connect(func_kf,'valueOut',reacF,'setNumKf')

Hmean.nInit = 0.0
theta.nInit = 0.0001

rdes = rd.rdesigneur(
    turnOffElec = True,
    #This subdivides the length of the soma into 0.5 micron voxels
    diffusionLength = diffL, 
    cellProto = [['somaProto', 'soma', 2*dendR, Length * 1e-6]],
    chemProto = [['cm', 'cm']],
    chemDistrib = [['cm', 'soma,dend#', 'install', '1' ]],
    plotList = [
        ['soma', '1', 'dend/Sphi', 'n', 'Surface fraction'],
        ['soma', '1', 'dend/fneq', 'n', 'noneq', 'num'],
        ['soma', '1', 'dend/feq', 'n', 'eq', 'num'],
        ['soma', '1', 'dend/full_pot', 'n', 'full_pot', 'wave'],
        ['soma', '1', 'dend/membrane', 'n', 'Mem', 'wave'],
    ],
    moogList = [['soma', '1', 'dend/membrane', 'conc', 'membrane Conc', 0, 20 ]]
)

#moose.setClock(0,0.1)
#moose.setClock(1,0.1)
#moose.setClock(2,0.1)
#moose.setClock(4,0.1)
#moose.setClock(8,0.1)

#moose.setClock(18,0.5)
moose.setClock(30, 0.1)   #PyRun clock tick 30
moose.setClock(10, 0.1)
#moose.setClock(15, 0.1)
#moose.setClock(16, 0.1)

#Initializing concentration on the membrane as 15% of the cytosol
#1L = 1e-3 m^3
#Here volume is in m^3
#concentration in litre units for IRSp53 is 1.2e-6 Molar
#In m^3, this is 1.2e-3 mM

pos = ((Length*1e-6)/diffL)/2.0
Cinit = 1.2e-3/5.0
num_moles = Cinit * Vcyt
num_molecules = num_moles * 6.022e23
num_molecules = num_molecules * f_cm
print("N molecules", num_molecules)
cytosol.concInit = 0.0
membrane.nInit = 1.0


for i in range(11,18):
    moose.setClock(i,0.1)
rdes.buildModel()
#for i in range(10):
#  moose.element("/model/chem/dend/cytosol["+str(pos)+"]").nInit = 10
#  pos = pos + 1

py_code = moose.PyRun('/model/py_code')
py_code.initString = """ 
print("Hello") 
print(moose.element('/clock').currentTime)
"""

py_code.runString = """
x0 = [1.0, 0.5] 
if moose.element('/clock').currentTime == 0:
   cyt_diff = moose.element("/model/chem/dend/cytosol").diffConst
   pos = ((Length*1e-6)/diffL)/2.0
   number_elements = len(moose.vec("/model/chem/dend/cytosol").n)
   x_diff = np.arange(0,number_elements,1)
   x_diff = (x_diff - pos) * diffL
   y = diff_profile(x_diff, moose.element('/clock').currentTime + 0.001, cyt_diff)
   moose.vec("/model/chem/dend/cytosol").n = y
rmbounds = (0.01,5.0)
tbounds = (0.001,3.14)
nbounds = (1,10)
#print(moose.element('/clock').currentTime)
mem_vec=moose.vec("/model/chem/dend/membrane").n
basal_membrane = min(mem_vec)

#Double check.
#expon = find_exponent(basal_membrane)
#expon = float("1e" + str(expon))
#cond = [mem_vec > round((basal_membrane/expon),2) * expon + expon]

cond = [mem_vec > 0.1]
#print("MEMBRANE",mem_vec) 
choice = [mem_vec]
nonzero = np.select(cond,choice)
moose.vec('/model/chem/dend/threshold').n = nonzero
Nzero = np.nonzero(moose.vec("/model/chem/dend/threshold").n)
phitot = sum(nonzero)
len_non_zero = len(Nzero[0][:])
#print("number of non zero voxels",len_non_zero)
#print("phisat", phisat)
#print("khat",khat)
n=1
ret = optimize.minimize( total_energy, x0=x0, bounds = (rmbounds,tbounds),args=(phitot,phisat, khat, fs, n, k, k_agg),method = 'L-BFGS-B' )
rm = ret.x[0] * 1e-6
theta = ret.x[1]
rp = rm * 1e6 * np.sin(theta)
if moose.element('/clock').currentTime == 0.5:
    record_initial.append(moose.vec("/model/chem/dend/cytosol").n)
    record_initial.append(moose.vec("/model/chem/dend/membrane").n)
if moose.element('/clock').currentTime%10.0 ==  0.0:
   x, y = dendShape( theta, rp, rm * 1e6)
   record_shapex.append(x)
   record_shapey.append(y)
#print("Rd,theta",rm,theta)
new_voxel_area = 2 * np.pi * rm**2 * (1 - np.cos(theta))
Nzero = np.nonzero(moose.vec("/model/chem/dend/threshold").n)
#print("Threshold", moose.vec("/model/chem/dend/threshold").n )
temp = moose.vec("/model/chem/dend/f_m").n
temp[Nzero[0][:]] = new_voxel_area    #Since we are dividing both phitot and area by len(Nzero), len(Nzero) cancels out in the expression for phi. So we don't need to split area here
print("New Area",new_voxel_area)
print("Assigning temp", temp)
moose.vec("/model/chem/dend/f_m").n= temp
#print("Area Vector", moose.vec("/model/chem/dend/f_m").n)
temp = moose.vec("/model/chem/dend/Hmean").n
temp[Nzero[0][:]] = 1/rm
moose.vec("/model/chem/dend/Hmean").n = temp
temp = moose.vec("/model/chem/dend/threshold").n
temp[Nzero[0][:]] = phitot
moose.vec("/model/chem/dend/threshold").n = temp
#print("PHITOT",moose.vec("/model/chem/dend/threshold").n)
#print("PHI",moose.vec("/model/chem/dend/Sphi").n)
#print(moose.vec("/model/chem/dend/Hmean").n)
#print(moose.vec("/model/chem/dend/f_m").n)

#print("FNEQ",(1/0.06) * f_cm * moose.vec("/model/chem/dend/full_pot").n)
#print("Membrane",moose.vec("/model/chem/dend/membrane").n)
#print("cytosol",moose.vec("/model/chem/dend/cytosol").n)
#input()
print("PHI",max(moose.vec("/model/chem/dend/Sphi").n))
print("THRESHOLD",max(moose.vec("/model/chem/dend/threshold").n))
print("MemArea",max(moose.vec("/model/chem/dend/f_m").n))
print("HMEAN",max(moose.vec("/model/chem/dend/Hmean").n))
record_R.append(rm/1e-6)
record_theta.append(theta)
record_area.append(new_voxel_area)
record_time.append(moose.element('/clock').currentTime)
record_phi.append(max(moose.vec("/model/chem/dend/Sphi").n))
record_old_area.append(voxel_area)
record_phitot.append(phitot)
record_kf.append(0.06 * f_cm * max(moose.vec("/model/chem/dend/full_pot").n))
record_sum_mem.append(sum(moose.vec("/model/chem/dend/membrane").n))
record_sum_cyt.append(sum(moose.vec("/model/chem/dend/cytosol").n))
"""


global record_R
global record_theta
global record_area
global record_phi
global record_time
global record_old_area
global record_phitot
global record_kf
global record_shapex
global record_shapey
global record_sum_mem
global record_sum_cyt
global record_initial
record_initial = []
record_sum_mem = []
record_sum_cyt = []
record_shapex = []
record_shapey = []
record_phitot = []
record_R=[]
record_theta=[]
record_area=[]
record_phi=[]
record_time=[]
record_old_area=[]
record_kf=[]
moose.reinit()
moose.start(4.79)
x=np.arange(0,Length,diffL/1e-6)
mem_vec = moose.vec("/model/chem/dend/membrane").n
df = pd.DataFrame()
mem_plot = plot_fn(x,mem_vec)
"""
plt.figure(101)
plt.plot(np.asarray(mem_plot['x']),np.asarray(mem_plot['vec']))
plt.xlabel("x $\mu m$")
plt.ylabel("Membrane - no: of molecules ")
plt.figure(102)
thresh_vec = moose.vec("/model/chem/dend/threshold").n
thresh_plot = plot_fn(x,thresh_vec)
plt.plot(np.asarray(thresh_plot['x']),np.asarray(thresh_plot['vec']))
plt.xlabel("x $\mu m$")
plt.ylabel("$\phi_{tot}$")
plt.figure(1020)
cyt_vec = moose.vec("/model/chem/dend/cytosol").n
cyt_plot = plot_fn(x,cyt_vec)
plt.plot(np.asarray(cyt_plot['x']),np.asarray(cyt_plot['vec']))
plt.show()
plt.figure(103)
plt.xlabel("Time (s)",fontsize=18)
plt.ylabel("R, $\Theta$",fontsize=18)
plt.plot(record_time,record_R,label="$R^{d}$")
plt.plot(record_time,record_theta,label="\Theta")
plt.legend()
plt.figure(104)
plt.xlabel("Time (s)",fontsize=18)
plt.ylabel("Area of dome",fontsize=18)
plt.plot(record_time, record_area,label="Area of dome")
plt.plot(record_time, record_old_area,label="Area of voxel before")
plt.legend()
plt.figure(105)
plt.xlabel("Time (s)",fontsize=18)
plt.ylabel("$\phi$",fontsize=18)
plt.plot(record_time,record_phi)
plt.figure(106)
plt.xlabel("Time (s)",fontsize=18)
plt.ylabel("$\phi_{tot}$",fontsize=18)
plt.plot(record_time,record_phitot)
plt.figure(107)
plt.xlabel("Time (s)",fontsize=18)
plt.ylabel("Kf",fontsize=18)
plt.plot(record_time,record_kf)
plt.figure(108)
plt.xlabel("x $\mu m$",fontsize=18)
plt.ylabel("y $\mu m$",fontsize=18)
for i in range(len(record_shapex)):
   plt.plot(record_shapex[i],record_shapey[i])
plt.figure(109)
plt.xlabel("Time (s)",fontsize=18)
plt.ylabel("num. molec. on mem")
plt.plot(record_time,record_sum_mem)
plt.figure(110)
plt.xlabel("Time (s)",fontsize=18)
plt.ylabel("num. molec. in cyt")
plt.plot(record_time,record_sum_cyt)
"""
#rdes.displayMoogli( 1, 50,rotation = 0, azim = -np.pi/2, elev = 0.0 )
